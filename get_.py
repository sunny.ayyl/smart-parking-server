import json
from os.path import abspath
import re
import datetime
import requests as req
# file1
json_file_path = abspath("car_data.json")
with open(json_file_path) as json_file:
    data = json.load(json_file)
# file2
json_file2_path = abspath("parking_data.json")
with open(json_file2_path) as json_file2:
    data2 = json.load(json_file2)


def get_data():
    with open(json_file_path) as json_file:
        data = json.load(json_file)
    data.reverse()
    return json.dumps(data)


def register_car(car_license, status, last_update):
    with open(json_file_path) as json_file:
        data = json.load(json_file)
    registed = False
    count = 0
    for car_data_i in data:
        if car_data_i["car_license"] == car_license:
            # print(data[count])a
            registed = True
            break
        else:
            count += 1
    if registed == True:
        data[count]["car_license"] = car_license
        data[count]["status"] = status
        data[count]["last_update"] = last_update
    else:
        data.append({"car_license": car_license,
                     "status": status, "last_update": last_update})
        """
if len(data) > 11:
data_ = data
# data_.reverse()
try:
   for data__ in range(len(data_)):
       if data_[data__]["status"] != "Enter":
           print(data__)
           data_.pop(data__)
           # data_.reverse()
           data = data_
except:
   pass
   """
    with open(json_file_path, "w") as json_file_write:
        json.dump(data, json_file_write)
    return ""


def get_parking_list():
    with open(json_file2_path) as json_file2:
        data2 = json.load(json_file2)
    return json.dumps(data2)


def customer_service(msg):
    msg = msg.lower()
    if msg == "hi" or msg == "hello":
        return "Hi,what can I help you?"
    elif "map" in msg and "not" in msg:
        return "You should allow GPS permissions"
    elif "car info" in msg and "not working" in msg or "car info" in msg and "not working" in msg:
        return "Please try to update the app or contact the developer"
    elif "how are you" == msg:
        return "I am fine,thanks"
    elif "car info" in msg:
        return "I will redirect you to car info page"

    elif "register octoupus card" == msg:
        return "Type -> register <car license> <Enter/Exit>"
    elif msg.split(" ")[0] == "register":
        try:
            msg_ = msg.split(" ")
            register_car(msg_[1], msg[2],
                         str(datetime.datetime.now()).replace(" ", "-"))
            return "Sucess"
        except Exception as e:
            return "Type -> register <octoupus card number> <Enter/Exit>"
            weather = req.get(
                "https://api.openweathermap.org/data/2.5/weather?id=1819729&appid=a03cf4b424ffa1976d1fc494ec38e0fa").text
    elif "weather" in msg and "today" in msg:
        weather = req.get(
            "https://api.openweathermap.org/data/2.5/weather?id=1819729&appid=a03cf4b424ffa1976d1fc494ec38e0fa").text
        weather_p = json.loads(weather)
        w_main = weather_p["weather"][0]["main"]
        w_wea = weather_p["main"]["temp"]
        w_wea = round(w_wea - 273.15, 2)
        return("It is a " + str(w_main) + " day today.\n" + "The temperature today is " + str(w_wea) + "°C")
    elif "time" in msg:
        return "The time in Hong Kong is {}".format(
            "".join(list(str(datetime.datetime.now().time())))[:5])
    elif "joke" in msg:
        return "One day, children eat their homeworks \nBecause their teacher said it was a piece of cake"
    else:
        return "I am not sure what do you mean."
