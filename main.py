from flask import Flask, send_from_directory, redirect
from flask_cors import cross_origin
import get_
import os
app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route("/get_car_info/")
@cross_origin()
def get_car_():
    return get_.get_data()


@app.route("/register_car/<car_license>/<status>/<last_update>")
@cross_origin()
def register_car_(car_license, status, last_update):
    return get_.register_car(car_license, status, last_update)


@app.route("/get_parking_list")
@cross_origin()
def get_parking_list():
    return get_.get_parking_list()


@app.route("/chatbot/<msg>")
@cross_origin()
def chatbot(msg):
    return get_.customer_service(msg)


@app.route("/chatbot/")
@cross_origin()
def _404_chatbot():
    return "I am not sure what do you mean."


@app.route("/update", methods=["POST"])
@cross_origin()
def update_():
    os.system("/home/sunny_ayyl_school_project/smart-parking-server/update.sh")
    return ""


@app.route("/smart-parking")
def install_app():
    file = os.path.join("", "file")
    return send_from_directory(file, "smart-parking.apk")


@app.route("/install")
def install():
    return redirect("http://35.238.234.50/smart-parking")


if __name__ == "__main__":
    app.run(port=80, host="0.0.0.0")
